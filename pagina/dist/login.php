<?php
include('../src/php/config.php');
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>KroneOffice</title>
    <link href="css/styles.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
</head>

<body class="bg-primary">
    <div id="layoutAuthentication">
        <div id="layoutAuthentication_content" style="background-image: url('https://www.floridauniversitaria.es/es-ES/floridauniversitaria/PublishingImages/fachada_florida.jpg'); background-size: cover;">
            <main>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-5">
                            <div class="card shadow-lg border-0 rounded-lg mt-5">
                                <div class="card-header">
                                    <h3 class="text-center font-weight-light my-4"><?php echo $lang['signin'] ?></h3>
                                </div>
                                <div class="card-body">
                                    <form method="POST" action="../src/php/validate.php">
                                        <div class="form-group"><label class="small mb-1" for="inputEmailAddress"><?php echo $lang['username'] ?></label><input class="form-control py-4" id="inputEmailAddress" name="user" type="text" placeholder="<?php echo $lang['user_info'] ?>" /></div>
                                        <div class="form-group"><label class="small mb-1" for="inputPassword"><?php echo $lang['pass'] ?></label><input class="form-control py-4" id="inputPassword" name="pass" type="password" placeholder="<?php echo $lang['pass_info'] ?>" /></div>
                                        <div class="form-group">
                                            <div class="custom-control custom-checkbox"><input class="custom-control-input" id="rememberPasswordCheck" type="checkbox" /><label class="custom-control-label" for="rememberPasswordCheck"><?php echo $lang['pass_remember'] ?></label></div>
                                        </div>
                                        <div class="form-group d-flex align-items-center justify-content-between mt-4 mb-0"><button type="submit" class="btn btn-primary"><?php echo $lang['login'] ?></button></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div id="layoutAuthentication_footer">
            <footer class="py-4 bg-light mt-auto">
                <div class="container-fluid">
                    <div class="d-flex align-items-center justify-content-between small">
                        <div class="text-muted">Copyright &copy; Florida Universitaria & Krone</div>
                        <div>
                            <div class="dropdown">
                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                    <?php echo $lang['lang'] ?>
                                </button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item translate" id="en" href="login.php?lang=en"> <?php echo $lang['en'] ?> </a>
                                    <a class="dropdown-item translate" id="ca" href="login.php?lang=ca"> <?php echo $lang['ca'] ?> </a>
                                    <a class="dropdown-item translate" id="es" href="login.php?lang=es"> <?php echo $lang['es'] ?> </a>
                                    <a class="dropdown-item translate" id="eu" href="login.php?lang=eu"> <?php echo $lang['eu'] ?> </a>
                                    <a class="dropdown-item translate" id="ga" href="login.php?lang=ga"> <?php echo $lang['ga'] ?> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="js/scripts.js"></script>
</body>

</html>