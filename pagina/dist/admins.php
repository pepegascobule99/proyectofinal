<?php
include('../src/php/config.php');
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>KroneOffice</title>
    <link href="css/styles.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
    <script src="https://code.iconify.design/1/1.0.4/iconify.min.js"></script>
</head>

<body class="sb-nav-fixed">
    <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
        <a class="navbar-brand" href="tables.php" style="width: 50px;"><img src="https://pbs.twimg.com/profile_images/921299071880622080/5lhYhzb6_400x400.jpg" alt="Logo" style="width:40px;"></a><span class="navbar-brand">KroneOffice</span><button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button>
    </nav>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                <div class="sb-sidenav-menu">
                    <div class="nav">
                        <div class="sb-sidenav-menu-heading">Data</div>
                        <a class="nav-link" href="charts.php">
                            <div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>
                            <?php echo $lang['chart'] ?>
                        </a><a class="nav-link" href="tables.php">
                            <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                            <?php echo $lang['es_users'] ?>
                        </a><a class="nav-link" href="admins.php">
                            <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                            Admin
                        </a>
                    </div>
                </div>
            </nav>
        </div>
        <div id="layoutSidenav_content">
            <main>
                <div class="container-fluid">
                    <h1 class="mt-4"><?php echo $lang['g_admin'] ?></h1>
                    <br>
                    <div class="card mb-4">
                        <div class="card-header"><i class="fas fa-table mr-1"></i>Admins</div>
                        <div class="modal fade" id="modalRegisterForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header text-center">
                                        <h4 class="modal-title w-100 font-weight-bold"><?php echo $lang['new_admin'] ?></h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form method="POST" action="../src/php/newAdmin.php">
                                        <div class="modal-body mx-3">

                                            <div class="form-group"><label class="small mb-1" for="inputUserAddress">User</label><input class="form-control py-4" id="inputUserAddress" name="user" type="text" placeholder="Enter user" /></div>
                                            <div class="form-group"><label class="small mb-1" for="inputPassword">Password</label><input class="form-control py-4" id="inputPassword" name="pass" type="password" placeholder="Enter password" /></div>
                                            <div class="form-group"><label class="small mb-1" for="inputNameAddress">User</label><input class="form-control py-4" id="inputNameAddress" name="name" type="text" placeholder="Enter name" /></div>
                                            <div class="form-group"><label class="small mb-1" for="inputSurname">Surname</label><input class="form-control py-4" id="inputSurname" name="surname" type="text" placeholder="Enter surname" /></div>
                                            <div class="form-group"><label class="small mb-1" for="inputDNI">DNI</label><input class="form-control py-4" id="inputDNI" name="dni" type="text" placeholder="Enter DNI" /></div>

                                        </div>
                                        <div class="modal-footer d-flex justify-content-center">
                                            <div class="form-group d-flex align-items-center justify-content-between mt-4 mb-0"><button type="submit" class="btn btn-primary addUser">Add User</button></div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <button data-toggle="modal" data-target="#modalRegisterForm" type="button" class="btn btn-success"><?php echo $lang['new_admin'] ?></button>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th><?php echo $lang['user'] ?></th>
                                            <th><?php echo $lang['name'] ?></th>
                                            <th><?php echo $lang['surname'] ?></th>
                                            <th>DNI</th>
                                            <th><?php echo $lang['actions'] ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        include('../src/php/userAdmin.php');
                                        if ($result > 0) {
                                            foreach ($result as $data) {
                                        ?>
                                                <tr>
                                                    <td id="id-<?php echo $data['id'] ?>"><?php echo $data['id'] ?></td>
                                                    <td id="user-<?php echo $data['id'] ?>"><?php echo $data['user'] ?></td>
                                                    <td id="name-<?php echo $data['id'] ?>"><?php echo $data['name'] ?></td>
                                                    <td id="surname-<?php echo $data['id'] ?>"><?php echo $data['surname'] ?></td>
                                                    <td id="DNI-<?php echo $data['id'] ?>"><?php echo $data['DNI'] ?></td>
                                                    <td id="act-<?php echo $data['id'] ?>" data-user="<?php echo $data['id'] ?>">
                                                        <a class="delete" href="#"><span class="iconify" data-icon="dashicons:table-row-delete" data-inline="false" width="25" height="25"></span> <?php echo $lang['delete'] ?></a>
                                                    </td>
                                                </tr>
                                        <?php
                                            };
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
            <footer class="py-4 bg-light mt-auto">
                <div class="container-fluid">
                    <div class="d-flex align-items-center justify-content-between small">
                        <div class="text-muted">Copyright &copy; Florida Universitaria & Krone</div>
                        <div>
                            <div class="dropdown">
                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                    <?php echo $lang['lang'] ?>
                                </button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item translate" id="en" href="admins.php?lang=en"> <?php echo $lang['en'] ?> </a>
                                    <a class="dropdown-item translate" id="ca" href="admins.php?lang=ca"> <?php echo $lang['ca'] ?> </a>
                                    <a class="dropdown-item translate" id="es" href="admins.php?lang=es"> <?php echo $lang['es'] ?> </a>
                                    <a class="dropdown-item translate" id="eu" href="admins.php?lang=eu"> <?php echo $lang['eu'] ?> </a>
                                    <a class="dropdown-item translate" id="ga" href="admins.php?lang=ga"> <?php echo $lang['ga'] ?> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="js/scripts.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
    <script src="assets/charts/datatables.js"></script>

    <script>
        $('.delete').click(function() {
            var user_id = $(this).closest('td').data('user');
            console.log(user_id);
            $.ajax({
                url: '../src/php/deleteAdmin.php',
                data: {
                    "user_id": user_id
                },
                type: 'post',
                success: function() {
                    location.reload(true);
                }
            });
        });
    </script>
</body>

</html>