<?php
include('../src/php/selectCanvas.php');

rsort($ventas_mes);

// $ventas_mes = array_slice($ventas_mes, 0, 3, true);

?>

<script>
  // Set new default font family and font color to mimic Bootstrap's default styling
  Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
  Chart.defaults.global.defaultFontColor = '#292b2c';

  // Pie Chart Example
  var ctx = document.getElementById("myPieChart");
  var aperturasTotales = <?php echo json_encode($datos) ?>;
  // console.log(aperturasTotales);
  var lastOpen = <?php echo json_encode($ventas_mes) ?>;
  console.log(lastOpen);

  var labels = <?php echo json_encode($labels) ?>;
  console.log(labels);

  // console.log(aperturas);



  var myPieChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: labels,
      datasets: [{
        data: lastOpen,
        backgroundColor: ['#007bff', '#dc3545', '#ffc107', '#28a745'],
      }],
    },
  });
</script>