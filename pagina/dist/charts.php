<?php
include('../src/php/config.php');
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>KroneOffice</title>
    <link href="css/styles.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>

</head>

<body class="sb-nav-fixed">
    <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
        <a class="navbar-brand" href="tables.php" style="width: 50px;"><img src="https://pbs.twimg.com/profile_images/921299071880622080/5lhYhzb6_400x400.jpg" alt="Logo" style="width:40px;"></a><span class="navbar-brand">KroneOffice</span><button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button>
    </nav>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                <div class="sb-sidenav-menu">
                    <div class="nav">
                        <div class="sb-sidenav-menu-heading">Data</div>
                        <a class="nav-link" href="charts.php">
                            <div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>
                            <?php echo $lang['chart'] ?>
                        </a><a class="nav-link" href="tables.php">
                            <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                            <?php echo $lang['es_users'] ?>
                        </a><a class="nav-link" href="admins.php">
                            <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                            Admin
                        </a>
                    </div>
                </div>
            </nav>
        </div>
        <div id="layoutSidenav_content">
            <main>
                <div class="container-fluid">
                    <h1 class="mt-4">Control de Ventas</h1>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="card mb-4">
                                <div class="card-header"><i class="fas fa-chart-bar mr-1"></i>Total de Ventas</div>
                                <div class="card-body"><canvas id="myBarChart" width="100%" height="50"></canvas></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="card mb-4">
                                <div class="card-header"><i class="fas fa-chart-pie mr-1"></i>Tiendas con mas ventas este mes.</div>
                                <div class="card-body"><canvas id="myPieChart" width="100%" height="50"></canvas></div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
            <?php
            include("assets/charts/chart-bar.php");
            include("assets/charts/chart-pie.php");
            ?>
            <footer class="py-4 bg-light mt-auto">
                <div class="container-fluid">
                    <div class="d-flex align-items-center justify-content-between small">
                        <div class="text-muted">Copyright &copy; Florida Universitaria & Krone</div>
                        <div>
                            <div class="dropdown">
                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                    <?php echo $lang['lang'] ?>
                                </button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item translate" id="en" href="charts.php?lang=en"> <?php echo $lang['en'] ?> </a>
                                    <a class="dropdown-item translate" id="ca" href="charts.php?lang=ca"> <?php echo $lang['ca'] ?> </a>
                                    <a class="dropdown-item translate" id="es" href="charts.php?lang=es"> <?php echo $lang['es'] ?> </a>
                                    <a class="dropdown-item translate" id="eu" href="charts.php?lang=eu"> <?php echo $lang['eu'] ?> </a>
                                    <a class="dropdown-item translate" id="ga" href="charts.php?lang=ga"> <?php echo $lang['ga'] ?> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="js/scripts.js"></script>
</body>

</html>