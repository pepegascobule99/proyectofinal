<?php
$user_id = $_POST["user_id"];
$ban = $_POST["ban"];
$changeban = "";
if ($ban == 0) {
    $changeban = 1;
} else {
    $changeban = 0;
}

const LOCAL_SERVER_URL = 'http://localhost/ProyectoFinal/server/api/';

$data = array(
    "user" => $user_id,
    "ban" => $changeban,
);

$data = json_encode($data);

$curl = curl_init();

$url = LOCAL_SERVER_URL . 'update/ban/';
curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

// Optional Authentication:
// curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
// curl_setopt($curl, CURLOPT_USERPWD, "username:password");

curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);


$result = curl_exec($curl);

var_dump($result);

curl_close($curl);

// $result = json_decode($result, true);

// var_dump($result);
