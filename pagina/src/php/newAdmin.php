
<?php
$user = $_POST["user"];
$pass = $_POST["pass"];
$name = $_POST["name"];
$surname = $_POST["surname"];
$dni = $_POST['dni'];

const LOCAL_SERVER_URL = 'http://localhost/ProyectoFinal/server/api/';

$data = array(
    "user" => $user,
    "password" => $pass,
    "name" => $name,
    "surname" => $surname,
    "DNI" => $dni
);

$data = json_encode($data);

$curl = curl_init();

$url = LOCAL_SERVER_URL . 'create/admin/';
curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

// Optional Authentication:
// curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
// curl_setopt($curl, CURLOPT_USERPWD, "username:password");

curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);


$result = curl_exec($curl);

curl_close($curl);

if ($result == 1) {
    header("Location: ../../dist/admins.php");
    // echo "Bienvenido:" . $user;
} else {
    // header("Location: ../index.php");
    echo "<script>
    alert('Este usuario ya existe.');
    window.location= '../../dist/admins.php'
    </script>";
};

// $result = json_decode($result, true);
