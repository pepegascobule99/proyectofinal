<?php
session_start();

if (!isset($_SESSION['lang'])) {
    $_SESSION['lang'] = "en";
} elseif (isset($_GET['lang']) && $_SESSION['lang'] != $_GET['lang'] && !empty($_GET['lang'])) {
    switch ($_GET['lang']) {
        case 'en':
            $_SESSION['lang'] = "en";
            break;
        case 'es':
            $_SESSION['lang'] = "es";
            break;
        case 'eu':
            $_SESSION['lang'] = "eu";
            break;
        case 'ca':
            $_SESSION['lang'] = "ca";
            break;
        case 'ga':
            $_SESSION['lang'] = "ga";
            break;

        default:
            $_SESSION['lang'] = "en";
            break;
    }
}

require_once('locale/' . $_SESSION['lang'] . '.php');
