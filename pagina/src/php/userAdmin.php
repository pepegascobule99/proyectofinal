
<?php
// include('bd/connect_bd.php');
// $query = mysqli_query($conn, "SELECT u.id, u.user, u.name, u.surname, u.DNI, u.ban FROM user u");
// $result  = mysqli_num_rows($query);
// $conn->close();

const LOCAL_SERVER_URL = 'http://localhost/ProyectoFinal/server/api/';

$data = array(
    "user" => "-1"
);

$data = json_encode($data);

$curl = curl_init();

$url = LOCAL_SERVER_URL . 'read/admin/';
curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

// Optional Authentication:
// curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
// curl_setopt($curl, CURLOPT_USERPWD, "username:password");

curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);


$result = curl_exec($curl);

curl_close($curl);

$result = json_decode($result, true);

// var_dump($result);
