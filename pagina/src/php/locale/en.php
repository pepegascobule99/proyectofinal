<?php
$lang = array(
    "signin" => 'Sign In',
    "username" => 'Username',
    "pass" => 'Password',
    "login" => 'Login',
    "user_error" => 'Username is required',
    "user_info" => 'Enter Username',
    "pass_error" => 'Password is required',
    "pass_info" => 'Enter password',
    'pass_remember' => 'Remember password',
    'chart' => 'charts',
    'es_users' => 'User status',
    'user' => 'Usuari',
    'name' => 'Name',
    'surname' => 'Surname',
    'actions' => 'Actions',
    'status' => 'Status',
    'n_blocked' => 'Not blocked',
    'blocked' => 'Blocked',
    'delete' => 'Delete',
    'g_admin' => 'Management of Administrators',
    'new_admin' => 'New admin',



    // IDIOMA
    "lang" => 'Language',
    "en" => 'English',
    "ca" => 'Catalan',
    "es" => 'Spanish',
    "eu" => 'Basque',
    "ga" => 'Galician'
    // ---------------
);
