<?php
$lang = array(
    "signin" => 'Identificate',
    "username" => 'Usuario',
    "pass" => 'Contrasinal',
    "login" => 'Iniciar sesión',
    "user_error" => 'Requírese nome de usuario',
    "user_info" => 'Introduza o seu nome de usuario',
    "pass_error" => 'Requírese contrasinal',
    "pass_info" => 'Introducir o contrasinal',
    'pass_remember' => ' Pasahitza  gogoratu',
    'chart' => 'Gráficos',
    'es_users' => 'Estado de usuarios',
    'user' => 'Usuario',
    'name' => 'Nome',
    'surname' => 'Apelidos',
    'actions' => 'Accións',
    'status' => 'Estado',
    'n_blocked' => 'Non Bloqueado',
    'blocked' => 'Bloqueado',
    'delete' => 'Eliminar',
    'g_admin' => 'Xestión de administradores',
    'new_admin' => 'Novo  admin',


    // IDIOMA
    "lang" => 'Idioma',
    "en" => 'Ínguas',
    "ca" => 'Catalán',
    "es" => 'Español',
    "eu" => 'Eúscaro',
    "ga" => 'Gallego'
    // ---------------
);
