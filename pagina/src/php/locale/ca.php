<?php
$lang = array(
    "signin" => 'Identificat',
    "username" => 'Usuari',
    "pass" => 'Contrasenya',
    "login" => 'Inicia sessió',
    "user_error" => 'El nom d’usuari és obligatori',
    "user_info" => 'Introduïu nom d\'usuari',
    "pass_error" => 'Es requereix contrasenya',
    "pass_info" => 'Introduir la contrasenya',
    'pass_remember' => 'Recordar contrasenya',
    'chart' => 'Gràfics',
    'es_users' => 'Estat d\'usuaris',
    'user' => 'Usuari',
    'name' => 'Nom',
    'surname' => 'Cognoms',
    'actions' => 'Accions',
    'status' => 'Estat',
    'n_blocked' => 'No bloquejat',
    'blocked' => 'Bloquejat',
    'delete' => 'Eliminar',
    'g_admin' => 'Gestió d\'Administradors',
    'new_admin' => 'Nou admin',

    // IDIOMA
    "lang" => 'Llenguatge',
    "en" => 'Anglès',
    "ca" => 'Català',
    "es" => 'Spanish',
    "eu" => 'Euskera',
    "ga" => 'Galician'
    // ---------------
);
