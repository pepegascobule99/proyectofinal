<?php
$lang = array(
    "signin" => 'Identificate',
    "username" => 'Usuario',
    "pass" => 'Contraseña',
    "login" => 'Iniciar sesión',
    "user_error" => 'Se requiere nombre de usuario',
    "user_info" => 'Introduzca su nombre de usuario',
    "pass_error" => 'Se requiere contraseña',
    "pass_info" => 'Introducir la contraseña',
    'pass_remember' => 'Recordar contraseña',
    'chart' => 'Gráficos',
    'es_users' => 'Estado de usuarios',
    'user' => 'Usuario',
    'name' => 'Nombre',
    'surname' => 'Apellidos',
    'actions' => 'Acciones',
    'status' => 'Status',
    'n_blocked' => 'No bloqueado',
    'blocked' => 'Blocked',
    'delete' => 'Eliminar',
    'g_admin' => 'Gestión de administradores',
    'new_admin' => 'Nuevo admin',


    // IDIOMA
    "lang" => 'Idioma',
    "en" => 'Ingles',
    "ca" => 'Catalán',
    "es" => 'Español',
    "eu" => 'Euskera',
    "ga" => 'Gallego'
    // ---------------
);
