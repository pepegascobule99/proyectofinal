<?php
$lang  = array(
    "signin" => 'Identifikatu',
    "username" => 'Erabiltzaile',
    "pass" => 'Pasahitza',
    "login" => 'Hasi saioa',
    "user_error" => 'Erabiltzaile izena beharrezkoa da',
    "user_info" => 'Idatzi Erabiltzailea',
    "pass_info" => 'Pasahitza beharrezkoa da',
    "pass_error" => 'Idatzi pasahitza',
    'pass_remember' => 'Pasahitza gogoratu',
    'chart' => 'Grafikoak',
    'es_users' => 'Erabiltzaileen egoera',
    'user' => 'Erabiltzailea',
    'name' => 'Izena',
    'surname' => 'Abizenak',
    'actions' => 'Ekintzak',
    'status' => 'Estatua',
    'n_blocked' => 'Blokeatu gabea',
    'blocked' => 'Blokeatut',
    'delete' => 'Ezabatu',
    'g_admin' => 'Administratzailearen kudeaketa',
    'new_admin' => 'Administratzaile berria',


    // IDIOMA
    "lang" => 'Hizkuntza',
    "en" => 'English',
    "ca" => 'Katalanez',
    "es" => 'Espainiako',
    "eu" => 'Euskera',
    "ga" => 'Galiziako'
    // ---------------
);
