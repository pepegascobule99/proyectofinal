<?php
// include('bd/connect_bd.php');
// $query = mysqli_query($conn, "SELECT u.id, u.user, u.name, u.surname, u.DNI, u.ban FROM user u");
// $result  = mysqli_num_rows($query);
// $conn->close();

if (!defined('LOCAL_SERVER_URL')) {
    define("LOCAL_SERVER_URL", "http://localhost/ProyectoFinal/server/api/");
}


$dataSend = array(
    "id_tienda" => "-1"
);

$dataSend = json_encode($dataSend);

$curl = curl_init();

$url = LOCAL_SERVER_URL . 'read/tiendas/';
curl_setopt($curl, CURLOPT_POSTFIELDS, $dataSend);

// Optional Authentication:
// curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
// curl_setopt($curl, CURLOPT_USERPWD, "username:password");

curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);


$result = curl_exec($curl);

curl_close($curl);

$result = json_decode($result, true);

// var_dump($result);

$datos = array();
$labels = array();
$ventas_mes = array();

foreach ($result as $data) {
    $datos[$data['id_tienda']] = $data['ventas_totales'];
    $labels[] = $data['nombre_tienda'];
    $ventas_mes[$data['id_tienda']] = $data['ventas_mes'];
}

// var_dump($datos);

// var_dump($labels);

// var_dump($ventas_mes);
