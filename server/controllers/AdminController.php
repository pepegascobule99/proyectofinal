<?php
include_once '../../../models/Admin.php';

class AdminController
{
    private $db;
    private $requestMethod;
    private $user;

    private $adminGateway;

    public function __construct($requestMethod)
    {

        $this->requestMethod = $requestMethod;

        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        if (isset($input['user'])) {
            $this->user = $input['user'];
        }

        $this->adminGateway = new Admin();
    }

    public function processRequest()
    {
        switch ($this->requestMethod) {
            case 'GET':
                if ($this->user != -1) {
                    $response = $this->getUser($this->user);
                } else {
                    $response = $this->getAllUsers();
                };
                break;
            case 'POST':
                $response = $this->createUserFromRequest();
                break;
            case 'PUT':
                $response = $this->updateUserFromRequest($this->user);
                break;
            case 'DELETE':
                $response = $this->deleteUser($this->user);
                break;
            case 'LOGIN':
                $response = $this->loginUser($this->user);
                break;
            default:
                $response = $this->notFoundResponse();
                break;
        }
        header($response['status_code_header']);
        if ($response['body']) {
            echo $response['body'];
        }
    }

    private function getAllUsers()
    {
        $result = $this->adminGateway->findAll();
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result);
        return $response;
    }

    private function getUser($id)
    {
        $result = $this->adminGateway->find($id);
        if (!$result) {
            return $this->notFoundResponse();
        }
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result);
        return $response;
    }

    private function loginUser($id)
    {
        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        $result = $this->adminGateway->login($input['user'], $input['password']);
        if (!$result) {
            return $this->notFoundResponse();
        }
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result[0]["COUNT(user)"]);
        return $response;
    }

    private function createUserFromRequest()
    {
        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        if (!$this->validatePerson($input)) {
            return $this->unprocessableEntityResponse();
        }
        $result = $this->adminGateway->insert($input['user'], $input['password'], $input['name'], $input['surname'], $input['DNI']);
        if ($result == 0) {
            $response['status_code_header'] = 'HTTP/1.1 406 Not Acceptable';
            $response['body'] = null;
            return $response;
        } else {
            $response['status_code_header'] = 'HTTP/1.1 201 Created';
            $response['body'] = 1;
            return $response;
        }
    }

    private function updateUserFromRequest($id)
    {
        $result = $this->adminGateway->find($id);
        if (!$result) {
            return $this->notFoundResponse();
        }
        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        if (!$this->validatePerson($input)) {
            return $this->unprocessableEntityResponse();
        }
        $this->adminGateway->update($input['user'], $input['password'], $input['name'], $input['surname'], $input['DNI']);
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = null;
        return $response;
    }

    private function deleteUser($id)
    {
        $result = $this->adminGateway->find($id);
        if (!$result) {
            return $this->notFoundResponse();
        }
        $this->adminGateway->delete($id);
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = null;
        return $response;
    }

    private function validatePerson($input)
    {
        if (!isset($input['user'])) {
            return false;
        }
        if (!isset($input['password'])) {
            return false;
        }
        if (!isset($input['name'])) {
            return false;
        }
        if (!isset($input['surname'])) {
            return false;
        }
        if (!isset($input['DNI'])) {
            return false;
        }
        return true;
    }

    private function unprocessableEntityResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 422 Unprocessable Entity';
        $response['body'] = json_encode([
            'error' => 'Invalid input'
        ]);
        return $response;
    }

    private function notFoundResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 404 Not Found';
        $response['body'] = null;
        return $response;
    }
}
