<?php
include_once '../../../models/Tiendas.php';

class TiendasController
{
    private $db;
    private $requestMethod;
    private $id_tienda ;

    private $tiendasGateway;

    public function __construct($requestMethod)
    {

        $this->requestMethod = $requestMethod;

        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        if (isset($input['id_tienda'])) {
            $this->id_tienda = $input['id_tienda'];
        }

        $this->tiendasGateway = new Tiendas();
    }

    public function processRequest()
    {
        switch ($this->requestMethod) {
            case 'GET':
                if ($this->id_tienda != -1) {
                    $response = $this->getTiendas($this->id_tienda);
                } else {
                    $response = $this->getAllTiendas();
                };
                break;
            case 'POST':
                $response = $this->createTiendasFromRequest();
                break;

            case 'DELETE':
                $response = $this->deleteTiendas($this->id_tienda);
                break;

            default:
                $response = $this->notFoundResponse();
                break;
        }
        header($response['status_code_header']);
        if ($response['body']) {
            echo $response['body'];
        }
    }

    private function getAllTiendas()
    {
        $result = $this->tiendasGateway->findAll();
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result);
        return $response;
    }

    private function getTiendas($id_tienda)
    {
        $result = $this->tiendasGateway->find($id_tienda);
        if (!$result) {
            return $this->notFoundResponse();
        }
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result);
        return $response;
    }

    private function createTiendasFromRequest()
    {
        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
 
        $result = $this->tiendasGateway->insert($input['nombre_tienda'], $input['ventas_totales'], $input['dinero_total_ganado'], $input['dinero_ganado_mes'], $input['ventas_mes']);
        if ($result == 0) {
            $response['status_code_header'] = 'HTTP/1.1 406 Not Acceptable';
            $response['body'] = null;
            return $response;
        } else {
            $response['status_code_header'] = 'HTTP/1.1 201 Created';
            $response['body'] = null;
            return $response;
        }
    }

    private function deleteTiendas($id_tienda)
    {
        $result = $this->tiendasGateway->find($id_tienda);
        if (!$result) {
            return $this->notFoundResponse();
        }
        $this->tiendasGateway->delete($id_tienda);
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = null;
        return $response;
    }

    private function notFoundResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 404 Not Found';
        $response['body'] = null;
        return $response;
    }
}
