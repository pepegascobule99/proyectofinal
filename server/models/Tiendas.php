<?php
require_once("DBController.php");
class Tiendas
{
    private $db_handle;

    public $id_tienda;
    public $nombre_tienda;
    public $ventas_totales;
    public $dinero_total_ganado;
    public $dinero_ganado_mes;
    public $ventas_mes;

    function __construct()
    {
        $this->db_handle = new DBController();
    }

    function insert($nombre_tienda, $ventas_totales, $dinero_total_ganado, $dinero_ganado_mes, $ventas_mes)
    {
        $query = "INSERT INTO tiendas (nombre_tienda, ventas_totales, dinero_total_ganado, dinero_ganado_mes, ventas_mes) VALUES (?,?,?,?,?)";
        $paramType = "sssss";
        $paramValue = array(
            $nombre_tienda,
            $ventas_totales,
            $dinero_total_ganado,
            $dinero_ganado_mes,
            $ventas_mes
        );

        $insertId = $this->db_handle->insert($query, $paramType, $paramValue);
        return $insertId;
    }

    function delete($id_tienda)
    {
        $query = "DELETE FROM tiendas WHERE id_tienda = ?";
        $paramType = "s";
        $paramValue = array(
            $id_tienda
        );
        $this->db_handle->update($query, $paramType, $paramValue);
    }

    function find($id_tienda)
    {
        $query = "SELECT * FROM tiendas WHERE id_tienda = ?";
        $paramType = "s";
        $paramValue = array(
            $id_tienda
        );

        $result = $this->db_handle->runQuery($query, $paramType, $paramValue);
        return $result;
    }

    function findAll()
    {
        $sql = "SELECT * FROM tiendas ORDER BY id_tienda";
        $result = $this->db_handle->runBaseQuery($sql);
        return $result;
    }
}
