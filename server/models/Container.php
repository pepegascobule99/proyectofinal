<?php
require_once ("DBController.php");
class Container{
    private $db_handle;

    public $id;
    public $name;
    
    function __construct() {
        $this->db_handle = new DBController();
    }
    
    function insert($name) {
        $query = "INSERT INTO container (name) VALUES (?)";
        $paramType = "s";
        $paramValue = array(
            $name
        );
        
        $insertId = $this->db_handle->insert($query, $paramType, $paramValue);
        return $insertId;
    }
    
    function update($name, $container_id) {
        $query = "UPDATE container SET name = ? WHERE id = ?";
        $paramType = "si";
        $paramValue = array(
            $name,
            $container_id
        );
        
        $this->db_handle->update($query, $paramType, $paramValue);
    }
    
    function delete($container_id) {
        $query = "DELETE FROM container WHERE id = ?";
        $paramType = "i";
        $paramValue = array(
            $container_id
        );
        $this->db_handle->update($query, $paramType, $paramValue);
    }
    
    function find($container_id) {
        $query = "SELECT * FROM container WHERE id = ?";
        $paramType = "i";
        $paramValue = array(
            $container_id
        );
        
        $result = $this->db_handle->runQuery($query, $paramType, $paramValue);
        return $result;
    }
    
    function findAll() {
        $sql = "SELECT * FROM container ORDER BY id";
        $result = $this->db_handle->runBaseQuery($sql);
        return $result;
    }
}

?>