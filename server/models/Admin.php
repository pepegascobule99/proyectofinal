<?php
require_once("DBController.php");
class Admin
{
    private $db_handle;

    public $id;
    public $user;
    public $password;
    public $name;
    public $surname;
    public $DNI;
    public $dateCreated;
    public $dateModified;


    function __construct()
    {
        $this->db_handle = new DBController();
    }

    function login($id, $pass)
    {
        $query = "SELECT COUNT(user) FROM admin WHERE user = ? AND password = ?";
        $paramType = "ss";
        $paramValue = array(
            $id,
            $pass
        );

        $result = $this->db_handle->runQuery($query, $paramType, $paramValue);
        return $result;
    }

    function insert($user, $password, $name, $surname, $DNI)
    {
        $query = "INSERT INTO admin (user, password, name, surname, DNI) VALUES (?,?,?,?,?)";
        $paramType = "sssss";
        $paramValue = array(
            $user,
            $password,
            $name,
            $surname,
            $DNI
        );

        $insertId = $this->db_handle->insert($query, $paramType, $paramValue);
        return $insertId;
    }

    function update($user, $password, $name, $surname, $DNI)
    {
        $query = "UPDATE admin SET password=?,name=?,surname=?,DNI=?,dateModified=? WHERE user = ?";
        $paramType = "ssssss";
        $paramValue = array(
            $password,
            $name,
            $surname,
            $DNI,
            date('Y-m-d H:i:s'),
            $user

        );

        $this->db_handle->update($query, $paramType, $paramValue);
    }

    function ban($user, $ban)
    {
        $query = "UPDATE admin SET ban=? WHERE user = ?";
        $paramType = "ss";
        $paramValue = array(
            $ban,
            $user

        );

        $this->db_handle->update($query, $paramType, $paramValue);
    }

    function delete($id)
    {
        $query = "DELETE FROM admin WHERE user = ?";
        $paramType = "s";
        $paramValue = array(
            $id
        );
        $this->db_handle->update($query, $paramType, $paramValue);
    }

    function find($id)
    {
        $query = "SELECT id, user, name, surname, DNI, ban, dateCreated, dateModified FROM admin WHERE user = ?";
        $paramType = "s";
        $paramValue = array(
            $id
        );

        $result = $this->db_handle->runQuery($query, $paramType, $paramValue);
        return $result;
    }

    function findAll()
    {
        $sql = "SELECT * FROM admin ORDER BY id";
        $result = $this->db_handle->runBaseQuery($sql);
        return $result;
    }
}
