-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-05-2020 a las 21:00:38
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `proyectofinal`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `user` varchar(11) NOT NULL,
  `password` varchar(11) NOT NULL,
  `name` varchar(11) NOT NULL,
  `surname` varchar(11) NOT NULL,
  `DNI` varchar(11) NOT NULL,
  `dateCreated` datetime NOT NULL DEFAULT current_timestamp(),
  `dateModified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `admin`
--

INSERT INTO `admin` (`id`, `user`, `password`, `name`, `surname`, `DNI`, `dateCreated`, `dateModified`) VALUES
(11, 'Usuario_1', 'qwerty', 'Usuario ', '1', '123456789A', '2020-03-24 13:36:03', '2020-03-24 13:36:04'),
(12, 'Usuario_2', 'qwerty', 'Usuario ', '2', '123456789B', '2020-03-26 10:42:45', '0000-00-00 00:00:00'),
(13, 'Usuario_3', 'qwerty', 'Usuario ', '3', '123456789C', '2020-03-26 10:45:58', '0000-00-00 00:00:00'),
(14, 'Usuario_4', 'qwerty', 'Usuario ', '4', '123456789D', '2020-03-26 10:50:12', '0000-00-00 00:00:00'),
(16, 'Usuario_5', 'qwerty', 'Usuario ', '5', '123456789E', '2020-03-26 10:52:02', '0000-00-00 00:00:00'),
(17, 'Usuario_6', 'qwerty', 'Usuario ', '6', '123456789F', '2020-03-26 10:54:16', '0000-00-00 00:00:00'),
(18, 'Usuario_7', 'qwerty', 'Usuario ', '7', '123456789G', '2020-03-26 10:54:49', '0000-00-00 00:00:00'),
(19, 'Usuario_8', 'qwerty', 'Usuario ', '8', '123456789H', '2020-03-26 10:57:29', '0000-00-00 00:00:00'),
(20, 'Usuario_9', 'qwerty', 'Usuario ', '9', '123456789Y', '2020-05-28 16:50:49', '0000-00-00 00:00:00'),
(21, 'test', 'test', 'test', 'test', 'test', '2020-05-28 20:08:49', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiendas`
--

CREATE TABLE `tiendas` (
  `id_tienda` int(11) NOT NULL,
  `nombre_tienda` varchar(50) NOT NULL DEFAULT 'Tienda sin nombre',
  `ventas_totales` varchar(50) NOT NULL,
  `dinero_total_ganado` varchar(50) NOT NULL DEFAULT '0',
  `dinero_ganado_mes` varchar(50) NOT NULL DEFAULT '0',
  `ventas_mes` varchar(50) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tiendas`
--

INSERT INTO `tiendas` (`id_tienda`, `nombre_tienda`, `ventas_totales`, `dinero_total_ganado`, `dinero_ganado_mes`, `ventas_mes`) VALUES
(1, 'Prueba de Tienda 1', '1500', '20', '30', '500'),
(2, 'Prueba de Tienda 2', '2050', '20', '30', '1050'),
(3, 'Prueba de Tienda 3', '575', '20', '30', '250'),
(4, 'Prueba de Tienda 4', '5000', '1000', '1000', '2500');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `user` varchar(11) NOT NULL,
  `password` varchar(11) NOT NULL,
  `name` varchar(11) NOT NULL,
  `surname` varchar(11) NOT NULL,
  `DNI` varchar(11) NOT NULL,
  `ban` tinyint(1) NOT NULL DEFAULT 0,
  `dateCreated` datetime NOT NULL DEFAULT current_timestamp(),
  `dateModified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `user`, `password`, `name`, `surname`, `DNI`, `ban`, `dateCreated`, `dateModified`) VALUES
(1, 'test', 'test', 'nombreTest', 'apeTest', '73594143P', 0, '2020-01-14 10:47:15', '2020-03-25 10:51:57'),
(7, 'Krone', 'qwerty', 'Pepe', 'Gasco', '54423051Z', 0, '2020-03-18 11:48:30', '2020-05-28 19:49:58'),
(10, 'sfadfdas', 'qwerty', 'Pepe', 'Gasco', '12345678A', 0, '2020-03-18 11:48:30', '2020-05-28 20:00:09');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `DNI` (`DNI`),
  ADD UNIQUE KEY `user` (`user`);

--
-- Indices de la tabla `tiendas`
--
ALTER TABLE `tiendas`
  ADD PRIMARY KEY (`id_tienda`) USING BTREE;

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `DNI` (`DNI`),
  ADD UNIQUE KEY `user` (`user`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `tiendas`
--
ALTER TABLE `tiendas`
  MODIFY `id_tienda` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
